/*
 * Pref.kt
 *
 * Copyright 2020 by MicMun
 */
package de.micmun.android.nextcloudcookbook.data

/**
 * Constants for pref name.
 *
 * @author MicMun
 * @version 1.2, 02.08.20
 */
class Pref {
   companion object {
      const val RECIPE_DIR = "recipe_directory"
      const val HIDDEN_FOLDER = "hidden_folder"
      const val THEME = "theme_setting"
      const val SORT = "recipe_list_sorting"
   }
}
